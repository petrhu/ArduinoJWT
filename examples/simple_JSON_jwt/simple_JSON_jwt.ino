/******* JWT *******/
/*
  ******************************************************
  ArduinoJWT(String psk);
  ArduinoJWT(char* psk);
  ******************************************************
  // Set a new psk for encoding and decoding JWTs
  void setPSK(String psk);
  void setPSK(char* psk);
  ******************************************************
  // Get the calculated length of a JWT
  int getJWTLength(String& payload);
  int getJWTLength(char* payload);
  ******************************************************
  // Get the length of the decoded payload from a JWT
  int getJWTPayloadLength(String& jwt);
  int getJWTPayloadLength(char* jwt);
  ******************************************************
  // Create a JSON Web Token
  String encodeJWT(String& payload);
  void encodeJWT(char* payload, char* jwt);
  ******************************************************
  // Decode a JWT and retreive the payload
  bool decodeJWT(String& jwt, String& payload);
  bool decodeJWT(char* jwt, char* payload, int payloadLength);
  ******************************************************
*/

#include <ArduinoJWT.h> //https://github.com/yutter/ArduinoJWT

String dev_key = "eNhomKou0CMJ694nK281vghbb6UtIQB2";
//String message = "{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";
String message = "{\"sensor\":\"gps\",\"time\":1351824120}";

ArduinoJWT jwt = ArduinoJWT(dev_key);

void setup()
{
  Serial.begin(57600);

  // The original Message
  Serial.println();
  Serial.println("1) message: ");
  Serial.println(message);
  Serial.println();
  //delay(1000);

  // JWT - Encode a JSON Web Token
  String encodedJWT = jwt.encodeJWT(message);
  Serial.println("2) encodedJWT: ");
  Serial.println(encodedJWT);
  Serial.println();
  //delay(1000);

  // Get the calculated length of a JWT
  int encodedJWTLength = jwt.getJWTLength(encodedJWT);
  Serial.println("3) encodedJWTLength: ");
  Serial.println(encodedJWTLength);
  Serial.println();
  //delay(1000);

  // Get the length of the decoded payload from a JWT
  int encodedJWTPayloadLength = jwt.getJWTPayloadLength(encodedJWT);
  Serial.println("4) encodedJWTPayloadLength: ");
  Serial.println(encodedJWTPayloadLength);
  //delay(1000);
  
  // JWT - Decode a JWT and retreive the payload
  String testDecoded = "x";
  for(int i=0 ; i<message.length() ; i++) testDecoded[i] += 'x';
  boolean decodedToken = jwt.decodeJWT(encodedJWT, testDecoded);
  Serial.println();
    
  //jwt.decodeJWT(encodedJWT, decodedPayload);
  Serial.println("5) testDecoded: ");
  Serial.println(testDecoded);
  delay(5000);
}

void loop()
{
    ;
}